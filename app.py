import json
from flask import Flask
from src.users.controller.user_controller import user_blueprint

app = Flask(__name__)


# Index route - test routing
@app.route('/', methods=['GET'])
def index():
    msg = {
        "message": "Routing works!"
    }
    return (
        json.dumps(msg),
        200,
        {'Content-Type': "application/json"}
    )


app.register_blueprint(user_blueprint)
