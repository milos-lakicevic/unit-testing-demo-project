import unittest
import uuid

import boto3
from moto import mock_dynamodb2


@mock_dynamodb2
class TestGetAllUsers(unittest.TestCase):
    pk1 = str(uuid.uuid4())
    pk2 = str(uuid.uuid4())
    pk3 = str(uuid.uuid4())
    pk4 = str(uuid.uuid4())

    user_payload_list = [
        {
            'pk': pk1,
            'username': 'John1',
            'first_name': 'John1',
            'last_name': 'Doe1',
            'email': 'john1@doe.com'
        },
        {
            'pk': pk2,
            'username': 'John2',
            'first_name': 'John2',
            'last_name': 'Doe2',
            'email': 'john2@doe.com'
        },
        {
            'pk': pk3,
            'username': 'John3',
            'first_name': 'John3',
            'last_name': 'Doe3',
            'email': 'john3@doe.com'
        },
        {
            'pk': pk4,
            'username': 'John4',
            'first_name': 'John4',
            'last_name': 'Doe4',
            'email': 'john4@doe.com'
        }
    ]

    def setUp(self):
        """
        Setting database resource and mock table
        """

        self.dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')
        table_name = 'test'

        self.table = self.dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': 'pk',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'pk',
                    'AttributeType': 'S'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )
        self.table.meta.client.get_waiter('table_exists').wait(TableName='test')
        assert self.table.table_status == 'ACTIVE'

        for item in self.user_payload_list:
            self.table.put_item(Item=item)

    def tearDown(self):
        """
        Deleting database resource and mock table
        """
        self.table.delete()
        self.dynamodb = None

    def testGetAllUsers(self):
        from src.users.repository.user_repository import findAll

        response = findAll(table=self.table)
        self.assertEqual(response, self.user_payload_list)
