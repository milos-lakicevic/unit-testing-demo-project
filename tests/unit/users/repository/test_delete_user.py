import unittest
import uuid

import boto3
from moto import mock_dynamodb2


@mock_dynamodb2
class TestUpdateUser(unittest.TestCase):
    pk = f"USER#{str(uuid.uuid4())}"

    user_payload = {
        'pk': pk,
        'username': 'John123',
        'first_name': 'John',
        'last_name': 'Doe',
        'email': 'john@doe.com'
    }

    def setUp(self):
        """
        Setting database resource and mock table
        """

        self.dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')
        table_name = 'test'

        self.table = self.dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': 'pk',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'pk',
                    'AttributeType': 'S'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )
        self.table.meta.client.get_waiter('table_exists').wait(TableName='test')
        assert self.table.table_status == 'ACTIVE'

        self.table.put_item(Item=self.user_payload)

    def tearDown(self):
        """
        Deleting database resource and mock table
        """
        self.table.delete()
        self.dynamodb = None

    def testDeleteUser(self):
        from src.users.repository.user_repository import delete

        response = delete(self.pk, table=self.table)
        self.assertEqual(
            response.get('message'),
            f"User with ID: {self.pk} is deleted successfully"
        )


if __name__ == '__main__':
    unittest.main()
