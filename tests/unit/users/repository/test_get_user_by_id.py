import unittest
import uuid

import boto3
from moto import mock_dynamodb2


@mock_dynamodb2
class TestGetUserByID(unittest.TestCase):
    pk = str(uuid.uuid4())

    user_payload = {
        'pk': f'USER#{pk}',
        'username': 'John1234',
        'first_name': 'John',
        'last_name': 'Doe',
        'email': 'john@doe.com'
    }

    def setUp(self):
        """
        Setting database resource and mock table
        """

        self.dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')
        table_name = 'test'

        self.table = self.dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': 'pk',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'pk',
                    'AttributeType': 'S'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )
        self.table.meta.client.get_waiter('table_exists').wait(TableName='test')
        assert self.table.table_status == 'ACTIVE'

        self.table.put_item(Item=self.user_payload)

    def tearDown(self):
        """
        Deleting database resource and mock table
        """
        self.table.delete()
        self.dynamodb = None

    def testGetUserByID(self):
        from src.users.repository.user_repository import find
        response = find(self.pk, table=self.table)
        self.assertEqual(response, self.user_payload)
