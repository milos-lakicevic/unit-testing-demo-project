import unittest

import boto3
from moto import mock_dynamodb2


@mock_dynamodb2
class TestCreateUser(unittest.TestCase):

    def setUp(self):
        """
        Setting database resource and mock table
        """

        self.dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')
        table_name = 'test'

        self.table = self.dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': 'pk',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'pk',
                    'AttributeType': 'S'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )
        self.table.meta.client.get_waiter('table_exists').wait(TableName='test')
        assert self.table.table_status == 'ACTIVE'

    def tearDown(self):
        """
        Deleting database resource and mock table
        """
        self.table.delete()
        self.dynamodb = None

    def testCreateUser(self):
        from src.users.repository.user_repository import save
        from src.users.model.user import User
        pk = 'USER#7833704e-782d-4eca-ac1f-f5bea9aebe23'
        user = User(
            pk,
            'John1234',
            'John',
            'Doe',
            'john@doe.com'
        )

        response = save(user, table=self.table)

        self.assertEqual(response.get('username'), 'John1234')
        self.assertEqual(response.get('first_name'), 'John')
        self.assertEqual(response.get('last_name'), 'Doe')
        self.assertEqual(response.get('email'), 'john@doe.com')


if __name__ == '__main__':
    unittest.main()
