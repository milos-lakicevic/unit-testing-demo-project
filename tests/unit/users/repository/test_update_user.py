import unittest
import uuid

import boto3
from moto import mock_dynamodb2


@mock_dynamodb2
class TestUpdateUser(unittest.TestCase):
    pk = str(uuid.uuid4())

    user_payload = {
        'pk': f"USER#{pk}",
        'username': 'John123',
        'first_name': 'John',
        'last_name': 'Doe',
        'email': 'john@doe.com'
    }

    def setUp(self):
        """
        Setting database resource and mock table
        """

        self.dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')
        table_name = 'test'

        self.table = self.dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': 'pk',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'pk',
                    'AttributeType': 'S'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )
        self.table.meta.client.get_waiter('table_exists').wait(TableName='test')
        assert self.table.table_status == 'ACTIVE'

        self.table.put_item(Item=self.user_payload)

    def tearDown(self):
        """
        Deleting database resource and mock table
        """
        self.table.delete()
        self.dynamodb = None

    def testUpdateUser(self):
        from src.users.repository.user_repository import update
        from src.users.model.user import User

        user = User(
            self.user_payload.get('pk'),
            f"{self.user_payload.get('username')}UPDATED",
            f"{self.user_payload.get('first_name')}UPDATED",
            f"{self.user_payload.get('last_name')}UPDATED",
            f"{self.user_payload.get('email')}UPDATED"
        )

        response = update(user, table=self.table)

        self.assertEqual(response.get('pk'), user.pk)
        self.assertEqual(response.get('username'), user.username)
        self.assertEqual(response.get('first_name'), user.first_name)
        self.assertEqual(response.get('last_name'), user.last_name)
        self.assertEqual(response.get('email'), user.email)


if __name__ == '__main__':
    unittest.main()
