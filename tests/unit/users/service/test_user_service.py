import unittest
from unittest import TestCase
from unittest.mock import patch

from src.users.service import user_service


def repositoryCreateUser(user, table=None):
    return user.toDict()


class TestCreateUser(TestCase):

    table = 'test'

    user_payload = {
        'username': 'John123',
        'first_name': 'John',
        'last_name': 'Doe',
        'email': 'john@doe.com'
    }

    @patch('src.users.service.user_service.save', repositoryCreateUser)
    def testServiceCreateUser(self):
        # with patch.object(ProductionClass, 'save', return_value=None) as mock_method:
        #     something = ProductionClass()
        #     something.save(self.user_payload, table=None)
        #     mock_method.assert_called_with(param1=x, param2=y, param3=z)
        response = user_service.createUser(self.user_payload, self.table)
        self.assertNotEqual(response.get('pk'), None)


if __name__ == '__main__':
    unittest.main()
