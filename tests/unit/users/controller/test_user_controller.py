from unittest import TestCase
import uuid

import json
import requests


class TestUserController(TestCase):
    # base URL
    API_URL = "http://localhost:5000"

    # user API route
    USERS_URL = f"{API_URL}/users"

    pk_hash = str(uuid.uuid4())
    pk = f"USER#{pk_hash}"
    USER_DATA = {
        'pk': pk,
        'username': 'John124',
        'first_name': 'John',
        'last_name': 'Doe',
        'email': 'john@doe.com'
    }

    def testCreateUser(self):
        response = requests.post(
            self.USERS_URL,
            json=self.USER_DATA
        )
        response_content = json.loads(response.content)
        self.assertTrue(requests.request)
        self.assertEqual(self.USER_DATA, response_content.get('body'))
        self.assertEqual(response_content.get('StatusCode'), 201)

    def testGetUserByID(self):
        response = requests.get(f"{self.USERS_URL}/{self.pk_hash}")
        response_content = json.loads(response.content)

        self.assertEqual(response_content.get('StatusCode'), 200)

    def testGetUserByWrongID(self):
        response = requests.get(f"{self.USERS_URL}/4bb22968-a5ad-4f4e-a125-3469768a87asr")
        response_content = json.loads(response.content)
        self.assertEqual(response_content.get('StatusCode'), 404)

    def testGetAllUsers(self):
        response = requests.get(self.USERS_URL)
        response_content = json.loads(response.content)
        self.assertEqual(response_content.get('StatusCode'), 200)

    def testUpdateUser(self):
        response = requests.patch(
            f"{self.USERS_URL}/{self.pk}",
            json=self.USER_DATA
        )
        response_content = json.loads(response.content)
        self.assertEqual(response_content.get('StatusCode'), 200)

    def testtDeleteUser(self):
        response = requests.delete(f"{self.USERS_URL}/{self.pk}")
        response_content = json.loads(response.content)
        self.assertEqual(response_content.get('StatusCode'), 200)


if __name__ == '__main__':
    tester = TestUserController()

    tester.testCreateUser()
    tester.testGetUserByID()
    tester.testGetAllUsers()
    tester.testUpdateUser()
    tester.testtDeleteUser()
