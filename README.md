## Unit testing Lambda AWS with Python Flask 

Table of content:

1. [Requirements](#requirements)
2. [Unit Testing Repository Level](#unit-testing-repository-level)
3. [Unit Testing Service Level](#unit-testing-service-level)
4. [Unit Testing Controller Level](#unit-testing-controller-level)

---

### Requirements

For unit testing, we will use:

- [**unittest**](https://docs.python.org/3/library/unittest.html "unittest - Unit testing framework"), Unit test framework, originally inspired by JUnit. 
- [**moto**](http://docs.getmoto.org/en/latest/ "Moto - Mock AWS Services"), a library that allows your tests to easily mock out AWS services.
- [**requests**](https://docs.python-requests.org/en/master/ "Requests - HTTP for Humans™"), simple HTTP library, which allows you to send HTTP requests extremely easy.


Unittest is the default testing framework that ships with Python's standard library.
Moto and Requests libraries need to be installed in order to use them.


---

### Unit Testing Repository Level  <span id="Section2"><span>

In this example of Users API, the business logic is as simple as it is possible. Example of a repository function is 
save() function, which is meant to save user data into database. 
And its code looks like this:
##### *user_repository.py* #####

	import boto3

	dynamodb = boto3.resource("dynamodb")

	def save(user, table=None):
		if table is None:
			table = dynamodb.Table('users')
		response = table.put_item(Item=user.toDict())
		if response['ResponseMetadata']['HTTPStatusCode'] == 200:
			return user.toDict()
		return {
    		"error": "User is not created"
		}




When testing this level of application, we need first to import ***unittest*** framework, ***boto3***, and from *moto* library to import ***mock_dynamodb2***, as we want to mock DynamoDB AWS Service.
Then, our class *TestCreateUser* needs to extends ***unittest.TestCase***.
After that, there are two main methods inside TestCreateUser class - ***SetUp(self)*** and ***TearDown(self)***.
In SetUp method, we basically need to setup our testing environment, i.e. create mock database table, but we can make bigger setup for testing, if we need that.
In TearDown method, we are just destroying mocking environment. 
Besides those two methods, we create our methods which will test some feature, in this example save() function from user repository. 
**The most important thing** when using mock_dynamodb2 is to ***decorate*** our class with ***@mock_dynamodb*** decorator, which tells that we are going to mock DynamoDB AWS Service. 
**The second most important thing*** is that external resources are imported bellow the decorator, preferably at the place where you need to use them.
And testing code may look like this:



##### *test\_create\_user.py* #####

	import unittest
	import boto3
	from moto import mock_dynamodb2
	@mock_dynamodb2
	class TestCreateUser(unittest.TestCase):
    	def setUp(self):
        	self.dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')

        	table_name = 'test'

        	self.table = self.dynamodb.create_table(

            	TableName=table_name,

            	KeySchema=[
                	{
                    	'AttributeName': 'pk',
                    	'KeyType': 'HASH'
	                }
	            ],
	            AttributeDefinitions=[
	                {
	                    'AttributeName': 'pk',
	                    'AttributeType': 'S'
	                }
	            ],
	            ProvisionedThroughput={
	                'ReadCapacityUnits': 5,
	                'WriteCapacityUnits': 5
	            }
	        )
	        self.table.meta.client.get_waiter('table_exists').wait(TableName='test')
	        assert self.table.table_status == 'ACTIVE'
	
	    def tearDown(self):
	        """
	        Deleting database resource and mock table
	        """
	        self.table.delete()
	        self.dynamodb = None
	
	    def testCreateUser(self):
	        from src.users.repository.user_repository import save
	        from src.users.model.user import User
	        pk = 'USER#7833704e-782d-4eca-ac1f-f5bea9aebe23'
	        user = User(
	            pk,
	            'John1234',
	            'John',
	            'Doe',
	            'john@doe.com'
	        )
	
	        response = save(user, table=self.table)
	
	        self.assertEqual(response.get('username'), 'John1234')
	        self.assertEqual(response.get('first_name'), 'John')
	        self.assertEqual(response.get('last_name'), 'Doe')
	        self.assertEqual(response.get('email'), 'john@doe.com')
	
	
	if __name__ == '__main__':
	    unittest.main()



---


### Unit Testing Service Level ###

For testing this level of our simple API, we are using **unittest.mock** library, which provides a powerful mechanism for mocking objects, and it is called ***patch()***.
It looks up an object in a given module and replaces it with a Mock. Usually, *patch()* is used as an decorator or as a context manager, to provide a scope in which we will mock the target object.
Our simple service ***createUser()*** method, which we will test is looking like this:

##### *user_service.py* #####

	import uuid
	
	from src.users.model.user import User
	from src.users.repository.user_repository import save
	
	
	def createUser(user_payload, table=None):
	    if 'pk' not in user_payload:
	        pk = 'USER#' + str(uuid.uuid4())
	        user_payload['pk'] = pk
	    user = User(
	        user_payload.get('pk'),
	        user_payload.get('username'),
	        user_payload.get('first_name'),
	        user_payload.get('last_name'),
	        user_payload.get('email')
	    )
	    return save(user, table=table)



And here we will test whether our service creates PK for user, for example, but in our real service there will be so many things to be tested.
Since we are using repository method ***save()*** in this service, in order to stay in the area of unit testing, we will need to mock everything that do not belong to user service. Our function ***repositoryCreateUser()*** is mock function for ***save()*** function. And the code looks like this:




##### *test\_user\_service.py* #####

	import unittest
	from unittest import TestCase
	from unittest.mock import patch
	
	from src.users.service import user_service
	
	#mock of repository save() function
	def repositoryCreateUser(user, table=None):
	    return user.toDict()
	
	
	class TestCreateUser(TestCase):
	
	    table = 'test'
	
	    user_payload = {
	        'username': 'John123',
	        'first_name': 'John',
	        'last_name': 'Doe',
	        'email': 'john@doe.com'
	    }
	
	    @patch('src.users.service.user_service.save', repositoryCreateUser)
	    def testServiceCreateUser(self):
			"""
				It can be used also as a context manager
			"""
	        # with patch.object(ProductionClass, 'save', return_value=None) as mock_method:
	        #     something = ProductionClass()
	        #     something.save(self.user_payload, table=None)
	        #     mock_method.assert_called_with(param1=x, param2=y, param3=z)
	        response = user_service.createUser(self.user_payload, self.table)
	        self.assertNotEqual(response.get('pk'), None)
	
	
	if __name__ == '__main__':
	    unittest.main()





---


### Unit Testing Controller Level ###

For unit testing controller level, in this example, ***requests*** library is used. It's a simple HTTP library, which allows us to sent HTTP/1.1 requests extremely easy.
The base controller code for creating user looks like this:


##### *user_controller.py* #####
	
	from flask import Blueprint, request
	
	from src.users.service import user_service
	
	user_blueprint = Blueprint('user_blueprint', __name__)
	
	# User routes - /users/ - user CRUD
	@user_blueprint.route('/users', methods=['POST'])
	def createUser():
	    user_payload = request.json
	
	    response = {
	        "body": user_service.createUser(user_payload),
	        "StatusCode": 201,
	        "Content-Type": "application/json"
	    }
	    return response
	


We simply get request sent via post method to this route, and than inside controller method just call user service method createUser(). 


In our test file, we are importing ***unittest***, *uuid*, ***json*** and ***requests***.
Class TestUserController() extends unittest.TestCase. And inside this class, we have few variables/constants which we would use further on:

- API_URL - base URL
- USERS_URL - URL for users API calls
- USER_DATA - simple payload to send to our controller inside a request


Then we have a testing method ***testCreateUser(self)***, which sends a request to our endpoint using:
		
		requests.[HERE_COMES_AN_HTTP_METHOD](
			[HERE_COMES_AN_ENDPOINT_URL],
			json=[HERE_COMES_REQUEST_DATA]
		)

in our case:
		
		response = requests.post(
			self.USERS_URL,
			json=self.USER_DATA
		)


and our test code looks like this:


##### *test_user_controller.py* #####

	import unittest
	import uuid
	
	import json
	import requests
	
	
	class TestUserController(unittest.TestCase):
	    # base URL
	    API_URL = "http://localhost:5000"
	
	    # user API route
	    USERS_URL = f"{API_URL}/users"
	
	    pk_hash = str(uuid.uuid4())
	    pk = f"USER#{pk_hash}"
	    USER_DATA = {
	        'pk': pk,
	        'username': 'John124',
	        'first_name': 'John',
	        'last_name': 'Doe',
	        'email': 'john@doe.com'
	    }
	
	    def testCreateUser(self):
	        response = requests.post(
	            self.USERS_URL,
	            json=self.USER_DATA
	        )
	        response_content = json.loads(response.content)
	        self.assertTrue(requests.request)
	        self.assertEqual(self.USER_DATA, response_content.get('body'))
	        self.assertEqual(response_content.get('StatusCode'), 201)


---


## Conslusion ##

This is a simple showcase of how you can write unit tests for an API. It may not be the best, and surely is not the only way of doing this. There are many other ways, but those are ones which I explored and used. 
In this demo project, I have covered three different ways to do testing for three different layers of the API. 
In the requirements section are provided links to the documentation of each framework and library which is crucial for testing. 
They have more functionality that I have used, but for such a simple project there were no need for many of them. For example, ***patch()*** decorator has an ***side_effect*** parameter which can work with exceptions and other side effects.

Also, there is one additional library that may be useful. It is ***coverage*** library. It is not shipped with Python by default, so you'll need to install it. Then, when you think that you are done with writing your tests, you can simply run coverage commands in your terminal:

	coverage report -m -i --omit=venv/*

	coverage html --omit=venv/*


And then (for Mac):
	
	open htmlcov/index.html

For Windows:
	
	start htmlcov/index.html


And in your browser will be opened HTML view of your code coverage. Code coverage means what amount of your code is 'covered' with your tests. The optimal number is >80%, but as much closer to 100% the chances that something breaks in production are lower.

![HTML Coverage](tests/images/report.png "HTML REPORT")

If you open specific report, you can see which lines are missing/excluded from tests or just partially tested:

![HTML Coverage Missing](tests/images/missing.png "HTML REPORT MISSING")


---


![TESTING](tests/images/testing.jpg "Am I testing my code, or is it testing me")


---