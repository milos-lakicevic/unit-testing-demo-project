import boto3

dynamodb = boto3.resource("dynamodb")


def save(user, table=None):
    if table is None:
        table = dynamodb.Table('users')
    response = table.put_item(Item=user.toDict())
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return user.toDict()
    return {
        "error": "User is not created"
    }


def find(user_id, table=None):
    if table is None:
        table = dynamodb.Table('users')
    pk = f"USER#{user_id}"
    user = table.get_item(Key={'pk': pk})
    if 'Item' not in user:
        return {
            "error": f"User with ID: {pk} does not exist"
        }
    return user['Item']


def findAll(table=None):
    if table is None:
        table = dynamodb.Table('users')
    users = table.scan()
    user_list = []
    for user in users['Items']:
        user_list.append(user)
    return user_list


def update(user, table=None):
    if table is None:
        table = dynamodb.Table('users')
    response = table.update_item(
        Key={
            'pk': user.pk
        },
        UpdateExpression="set username=:uname, first_name=:fname, last_name=:lname, email=:mail",
        ExpressionAttributeValues={
            ':uname': user.username,
            ':fname': user.first_name,
            ':lname': user.last_name,
            ':mail': user.email
        },
        ReturnValues="UPDATED_NEW"
    )
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        response['Attributes']['pk'] = user.pk
        return response['Attributes']
    return {
        "error": "User is not updated"
    }


def delete(user_id, table=None):
    if table is None:
        table = dynamodb.Table('users')
    table.delete_item(Key={'pk': f"USER#{user_id}"})
    message = {"message": f"User with ID: {user_id} is deleted successfully"}
    return message
