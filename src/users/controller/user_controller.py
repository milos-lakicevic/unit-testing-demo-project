from flask import Blueprint, request
import json

from src.users.service import user_service

user_blueprint = Blueprint('user_blueprint', __name__)


# User routes - /users/ - user CRUD
@user_blueprint.route('/users', methods=['POST'])
def createUser():
    user_payload = request.json
    response = {
        "body": user_service.createUser(user_payload),
        "StatusCode": 201,
        "Content-Type": "application/json"
    }
    return response


@user_blueprint.route('/users/<user_id>', methods=['GET'])
def getUserByID(user_id):
    result = user_service.getUserByID(user_id)
    status_code = 200
    if 'error' in result:
        status_code = 404
    response = {
        "body": result,
        "StatusCode": status_code,
        "Content-Type": "application/json"
    }
    return response


@user_blueprint.route('/users', methods=['GET'])
def getAllUsers():
    response = {
        "body": user_service.getAllUsers(),
        "StatusCode": 200,
        "Content-Type": "application/json"
    }
    return response


@user_blueprint.route('/users/<user_id>', methods=['PATCH'])
def updateUser(user_id):
    user_payload = request.json
    response = {
        "body": user_service.updateUser(user_id, user_payload),
        "StatusCode": 200,
        "Content-Type": "application/json"
    }
    return response


@user_blueprint.route('/users/<user_id>', methods=['DELETE'])
def deleteUser(user_id):
    response = {
        "body": user_service.deleteUser(user_id),
        "StatusCode": 200,
        "Content-Type": "application/json"
    }
    return response
