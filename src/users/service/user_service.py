import uuid

from src.users.model.user import User
from src.users.repository.user_repository import save, find, findAll, update, delete


def createUser(user_payload, table=None):
    if 'pk' not in user_payload:
        pk = 'USER#' + str(uuid.uuid4())
        user_payload['pk'] = pk
    user = User(
        user_payload.get('pk'),
        user_payload.get('username'),
        user_payload.get('first_name'),
        user_payload.get('last_name'),
        user_payload.get('email')
    )
    return save(user, table=table)


def getUserByID(user_id, table=None):
    return find(user_id, table)


def getAllUsers(table=None):
    return findAll(table)


def updateUser(user_id, user_payload, table=None):
    pk = 'USER#' + user_id
    user = User(
        pk,
        user_payload.get('username'),
        user_payload.get('first_name'),
        user_payload.get('last_name'),
        user_payload.get('email')
    )
    return update(user, table)


def deleteUser(user_id, table=None):
    return delete(user_id, table)
