class User:
    def __init__(self, pk, username, first_name, last_name, email):
        self.pk = pk
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.email = email

    def toString(self):
        return "PK: " + self.pk + ", USERNAME: " + self.username + ", FIRST NAME: " + self.first_name + \
               ", LAST NAME: " + self.last_name + ", EMAIL: " + self.email

    def toDict(self):
        user_data = {
            "pk": self.pk,
            "username": self.username,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email
        }
        return user_data
